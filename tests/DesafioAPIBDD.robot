*** Settings ***
Library         Selenium2Library
Resource        ../resources/DesafioAPI.robot

*** Test Cases ***
Cenário 1: Chamar api de filmes - Filme existente
  Dado que eu crie uma sessão
  Quando eu realizar uma chamada na API com um ID de um filme existente
  Então os dados do filme serão retornados

Cenário 2: Chamar api de filmes - Filme inexistente
  Dado que eu crie uma sessão
  Quando eu realizar uma chamada na API com um ID de um filme inexistente
  Então será retornado erro


*** Keywords ***
Dado que eu crie uma sessão
  Criar sessão

Quando eu realizar uma chamada na API com um ID de um filme existente
  Chamar api de filmes - Filme existente

Quando eu realizar uma chamada na API com um ID de um filme inexistente
  Chamar api de filmes - Filme inexistente

Então os dados do filme serão retornados
  Realizar validações

Então será retornado erro
  Realizar validações
