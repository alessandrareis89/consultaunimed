*** Settings ***
Library         Selenium2Library
Resource        ../resources/ConsultarEspecial.robot
Test TearDown   Fechar navegador

*** Test Cases ***
Cenário 1: Consultar especialidade e verificar que é a cidade é RJ
  Dado que eu estou na página da Unimed
  Quando eu realizar uma pesquisa por uma especialidade em um determinado estado
  Então a especialidade da cidade do RJ deve ser listada na página de resultados

Cenário 2: Consultar especialidade e verificar que a cidade não é SP
  Dado que eu estou na página da Unimed
  Quando eu realizar uma pesquisa por uma especialidade em um determinado estado
  Então a especialidade da cidade de SP deve ser listada na página de resultados


*** Keywords ***
Dado que eu estou na página da Unimed
  Abrir Navegador

Quando eu realizar uma pesquisa por uma especialidade em um determinado estado
  Acessar a guia Médico
  Informar a especialidade e pesquisar
  Informar a localidade
  Selecionar a unidade e continuar

Então a especialidade da cidade do RJ deve ser listada na página de resultados
  Realizar as validações em 3 páginas - RJ

Então a especialidade da cidade de SP deve ser listada na página de resultados
  Realizar as validações em 3 páginas - SP
