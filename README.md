# Automação
#### Projeto de automação da consulta de uma especialidade no site da Unimed e automação de uma API de consulta de filmes.

Instalações:
- Python 3
- Robotframework
- IDE de preferência
- ChromeWebDriver
- Libraries (libraries utilizadas no projeto:
  - robotframework-jsonlibrary
  - robotframework
  - robotframework-requests
  - strings
  - robotframework-selenium2library
  - selenium
  - certifi
  - robotframework-python
  - libcore
  - webdriver-manager)

Passos para rodar os testes:
- Clonar o repositório
- Acessar a pasta **consultaunimed/tests**
- Informar o comando **robot** e o nome do BDD em questão. Ex:  
`robot ConsultarEspecialBDD.robot`  
`robot DesafioAPIBDD.robot`  
- Caso deseje executar todos os testes da pasta, basta digitar o comando **robot .**
