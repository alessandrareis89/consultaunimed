*** Settings ***
Library     RequestsLibrary
Library     Collections
Library     JSONLibrary
Library     String
Resource  ../resources/Variaveis.robot

*** Keywords ***
Criar sessão
  Create Session         filme                http://www.omdbapi.com          disable_warnings=True
  ${headers}             Create Dictionary    content-type=application/json   accept=text/plain
  Set Global Variable    ${headers}

Chamar api de filmes - Filme existente
  ${response}            Get Request          filme      ?i=${idFilme}&apikey=${apiKey}
  ...                    headers=${headers}
  Set Global Variable    ${response}
  Log                    ${response.url}
  Log                    ${response.json()}
  Log To Console         \nFilme encontrado!

Chamar api de filmes - Filme inexistente
  ${response}            Get Request          filme      ?i=${idFilmeInexistente}&apikey=${apiKey}
  ...                    headers=${headers}
  Set Global Variable    ${response}
  Log                    ${response.url}
  Log                    ${response.json()}

Realizar validações
  Status Should Be   200      ${response}
  ${status}          Run Keyword And Return Status   Dictionary Should Contain Key    ${response.json()}    Error
  Run Keyword If    '${status}'=='True'   Exibir mensagem para filme inexistente
  ${title}           Get From Dictionary    ${response.json()}    Title
  ${year}            Get From Dictionary    ${response.json()}    Year
  ${language}        Get From Dictionary    ${response.json()}    Language
  Should Be True    '${title}'=='${titleOk}'
  Should Be True    '${year}'=='${yearOk}'
  Should Be True    '${language}'=='${languageOk}'

Exibir mensagem para filme inexistente
  Log To Console    \nEsse filme não existe!
  Pass Execution    Fim.
