*** Settings ***
Library   Selenium2Library
Library   String
Resource  ../resources/Variaveis.robot

*** Keywords ***
Abrir Navegador
  Log To Console  \nAcessando o site...
  Open Browser    ${site}    ${browser}

Fechar navegador
  Close Browser

Acessar a guia Médico
  Click Link     //*[@id="menuPrincipalItens"]/ul/li[2]/a

Informar a especialidade e pesquisar
  Wait Until Element Is Visible        id=campo_pesquisa
  Input Text        id=campo_pesquisa    ${especialidade}
  Click Button      id=btn_pesquisar

Informar a localidade
  Wait Until Element Is Visible    //*[@class="css-k71zgk"][1]
  Click Element    //*[@class="css-k71zgk"][1]
  Sleep            1s
  Click Element    //*[@class="css-k71zgk"][1]
  Press Keys       //*[@class="css-k71zgk"][1]           Rio de Janeiro
  Press Keys       //*[@class="css-k71zgk"][1]           TAB
  Press Keys       //*[@class="css-k71zgk"][1]           TAB
  Press Keys       //*/form/div/div[2]/div/div/div[1]    Rio de Janeiro
  Press Keys       //*/form/div/div[2]/div/div/div[1]    TAB
  Log To Console    Pesquisando a especialidade ${especialidade}...

Selecionar a unidade e continuar
  Sleep            1s
  Click Element    //*/label/div[1]/input
  Click Button     //*[@class="margin-top margin-bottom"]/button[@class="btn btn-success"]

Realizar as validações em 3 páginas - RJ
  Sleep  4s
  Log To Console          Verificando os resultados...
  :FOR   ${count}         IN RANGE   0   3
         Validar que os resultados são da especialidade escolhida
         Validar que os resultados são da cidade escolhida
         Click Element    //*/li[13]/a/i
         Sleep            2s
  END

Realizar as validações em 3 páginas - SP
  Sleep  4s
  Log To Console          Verificando os resultados...
  :FOR   ${count}         IN RANGE   0   3
         Validar que os resultados são da especialidade escolhida
         Validar que os resultados NÃO são da cidade de São Paulo
         Click Element    //*/li[13]/a/i
         Sleep            2s
  END

Validar que os resultados são da especialidade escolhida
  :FOR   ${count}            IN RANGE   1  21
         ${status1}          Run Keyword And Return Status    Element Should Contain    //*[@id="app"]/div/div/div[8]/div[2]/div[${count}]/div/div[2]/div/p/span[2]   ${especialidade}
         ${status2}          Run Keyword And Return Status    Element Should Contain    //*[@id="app"]/div/div/div[8]/div[2]/div[${count}]/div/div[2]/div/p/span[3]   ${especialidade}
         Run Keyword And Ignore Error    Should Be True      '${status1}'=='True' or '${status2}'=='True'
   END

Validar que os resultados são da cidade escolhida
  :FOR   ${count}   IN RANGE   1    21
         Element Should Contain     //*[@id="app"]/div/div/div[8]/div[2]/div[1]/div/div[2]/div/span/p  RJ
   END

Validar que os resultados NÃO são da cidade de São Paulo
  :FOR   ${count}   IN RANGE   1        21
         Element Should Not Contain     //*[@id="app"]/div/div/div[8]/div[2]/div[1]/div/div[2]/div/span/p  SP
   END
